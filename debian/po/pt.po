# Translation of horizon's debconf messages to european portuguese
# Copyright (C) 2013 THE horizon'S COPYRIGHT HOLDER
# This file is distributed under the same license as the horizon package.
#
# Américo Monteiro <a_monteiro@gmx.com>, 2013 - 2018.
msgid ""
msgstr ""
"Project-Id-Version: horizon 3:14.0.0-3\n"
"Report-Msgid-Bugs-To: horizon@packages.debian.org\n"
"POT-Creation-Date: 2018-08-30 11:59+0200\n"
"PO-Revision-Date: 2018-10-05 13:01+0000\n"
"Last-Translator: Américo Monteiro <a_monteiro@gmx.com>\n"
"Language-Team: Portuguese <>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 2.0\n"

#. Type: boolean
#. Description
#: ../openstack-dashboard-apache.templates:2001
msgid "Activate Dashboard and disable default VirtualHost?"
msgstr "Activar o Dashboard e desactivar o VirtualHost predefinido?"

#. Type: boolean
#. Description
#: ../openstack-dashboard-apache.templates:2001
msgid ""
"The Apache package sets up a default web site and a default page, configured "
"in /etc/apache2/sites-available/000-default.conf."
msgstr ""
"O pacote Apache define um sítio web predefinido e uma página predefinida, "
"configurada em /etc/apache2/sites-available/000-default.conf."

#. Type: boolean
#. Description
#: ../openstack-dashboard-apache.templates:2001
msgid ""
"If this option is not selected, Horizon will be installed using /horizon "
"instead of the webroot."
msgstr ""
"Se esta opção não for seleccionada, o Horizon será instalado usando /horizon "
"em vez da webroot."

#. Type: boolean
#. Description
#: ../openstack-dashboard-apache.templates:2001
msgid ""
"Choose this option to replace that default with the OpenStack Dashboard "
"configuration."
msgstr ""
"Escolha esta opção para substituir essa predefinição pela configuração "
"Dashboard do OpenStack."

#. Type: boolean
#. Description
#: ../openstack-dashboard-apache.templates:3001
msgid "Should the Dashboard use HTTPS?"
msgstr "Deve o Dashboard usar HTTPS?"

#. Type: boolean
#. Description
#: ../openstack-dashboard-apache.templates:3001
msgid ""
"Select this option if you would like Horizon to be served over HTTPS only, "
"with a redirection to HTTPS if HTTP is in use."
msgstr ""
"Seleccione esta opção se você desejar que o Horizon seja servido apenas por "
"HTTPS, com um redireccionamento para HTTPS se for usado HTTP."

#. Type: string
#. Description
#: ../openstack-dashboard.templates:2001
msgid "Allowed hostname(s):"
msgstr "Nome(s) de máquina(s) permitidos:"

#. Type: string
#. Description
#: ../openstack-dashboard.templates:2001
msgid ""
"Please enter the list of hostnames that can be used to reach your OpenStack "
"Dashboard server. This is a security measure to prevent HTTP Host header "
"attacks, which are possible even under many seemingly-safe web server "
"configurations."
msgstr ""
"Por favor insira a lista de nomes de máquinas que podem ser usadas para "
"alcançar o seu servidor OpenStack Dashboard. Isto é uma medida de segurança "
"para prevenir ataques pelo cabeçalho HTTP Host, os quais são possíveis "
"mesmo sob configurações aparentemente seguras do servidor web."

#. Type: string
#. Description
#: ../openstack-dashboard.templates:2001
msgid ""
"Enter values separated by commas. Any space will be removed, so you can add "
"some to make it more readable."
msgstr ""
"Insira os valores separados por vírgulas. Quaisquer espaços serão removidos, "
"por isso você pode adicioná-los para tornar mais legível."

#. Type: string
#. Description
#: ../openstack-dashboard.templates:2001
msgid ""
"Values in this list can be fully qualified names like \"www.example.com\", "
"in which case they will be matched against the request's Host header exactly "
"(case-insensitive, not including port). A value beginning with a period can "
"be used as a subdomain wildcard: \".example.com\" will match example.com, "
"www.example.com, and any other subdomain of example.com. A value of \"*\" "
"will match anything; in this case you are responsible for providing your own "
"validation of the Host header (perhaps in middleware; if so this middleware "
"must be listed first in MIDDLEWARE)."
msgstr ""
"Os valores desta lista podem ser nomes totalmente qualificados como "
"\"www.example.com\", que neste caso serão comparados exactamente com o "
"cabeçalho Host pedido (sensível a maiúsculas/minúsculas, não incluindo o "
"porto). Um valor começado com uma vírgula pode ser usado como uma "
"wildcard de sub-domínio: \".example.com\" irá fazer corresponder "
"example.com, www.example.com, e qualquer outro sub-domínio de example.com. "
"Um valor de \"*\" irá corresponder a tudo, neste caso você é o responsável de "
"providenciar a sua própria validação do cabeçalho Host (talvez em middleware; "
"se assim for, este middleware terá de ser listado primeiro em MIDDLEWARE)."

