#!/bin/sh

set -e

rebuild_static() {
	# Recreate directory with static links
	mkdir -p /var/lib/openstack-dashboard/static/js
	mkdir -p /var/lib/openstack-dashboard/static/css

	# Compress the JS and CSS with python-compressor and python-lesscpy
	if [ -x /usr/share/openstack-dashboard/manage.py ] ; then
		/usr/share/openstack-dashboard/manage.py collectstatic --noinput --clear
		/usr/share/openstack-dashboard/manage.py compress --force
	fi

	if [ -f /var/lib/openstack-dashboard/secret-key/.secret_key_store ]; then
		rm /var/lib/openstack-dashboard/secret-key/.secret_key_store
	fi
	chown -R www-data /var/lib/openstack-dashboard/secret-key /var/lib/openstack-dashboard/static
}

generate_config_file() {
	# Copy Horizon's local_settings.py in /etc/openstack-dashboard/
	mkdir -p /etc/openstack-dashboard
	DASHBOARD_LOCAL_SETTINGS='/etc/openstack-dashboard/local_settings.py'
	if ! [ -f "$DASHBOARD_LOCAL_SETTINGS" ] ; then
		cp /usr/share/openstack-dashboard/examples/local_settings.py $DASHBOARD_LOCAL_SETTINGS
	fi
	if [ -f "$DASHBOARD_LOCAL_SETTINGS" ] && grep -q 'django.utils.log.NullHandler' $DASHBOARD_LOCAL_SETTINGS; then
		sed -i 's/django.utils.log.NullHandler/logging.NullHandler/g' $DASHBOARD_LOCAL_SETTINGS
	fi

	# Handle the ALLOWED_HOSTS directive with debconf
	db_get horizon/allowed-hosts
	if [ -n "${RET}" ] ; then
		if echo x"${RET}" | grep -q '*' ; then
			sed -i "s/^[# \t]*ALLOWED_HOSTS[\t ]*=.*/ALLOWED_HOSTS = \[ '"'*'"' \]/" /etc/openstack-dashboard/local_settings.py
		else
			# Refrmat the output as: ALLOWED_HOSTS = [ 'host1.example.com', 'host2.example.com' ]
			ALLOWED_HOSTS="ALLOWED_HOSTS = \["
			for i in $(echo ${RET} | sed -e 's/ //g' -e "s|'||g" -e 's|"||g' | tr , \\n) ; do
				ALLOWED_HOSTS="${ALLOWED_HOSTS} '${i}',"
			done
			ALLOWED_HOSTS="${ALLOWED_HOSTS} \]"
			sed -i "s/^[# \t]*ALLOWED_HOSTS[\t ]*=.*/${ALLOWED_HOSTS}/" /etc/openstack-dashboard/local_settings.py
		fi
	fi
}

if [ "$1" = "configure" ] ; then
	. /usr/share/debconf/confmodule

	# Copy over the files to /etc/openstack-dashboard/local_settings.d
	# *once*, then never do it again (as users may decide to handle configuration
	# in a different way than we may think).
	if [ ! -e /usr/share/openstack-dashboard-debian-settings.d/copied ] ; then
		mkdir -p /etc/openstack-dashboard/local_settings.d
		for i in $(find /usr/share/openstack-dashboard-debian-settings.d -type f -iname '*.py') ; do
			BASENAME=$(basename $i)
			install -D $i /etc/openstack-dashboard/local_settings.d/${BASENAME}
		done
		touch /usr/share/openstack-dashboard-debian-settings.d/copied
	fi

	# Create a new horizon user ###
	if ! getent group horizon > /dev/null 2>&1 ; then
		addgroup --quiet --system horizon
	fi
	if ! getent passwd horizon > /dev/null 2>&1 ; then
		adduser --system \
			--home /var/lib/horizon \
			--quiet \
			--disabled-password \
			--group horizon
	fi

	# Handle the secret key stuff ###
	mkdir -p /var/lib/openstack-dashboard/secret-key
	chown www-data:www-data /var/lib/openstack-dashboard/secret-key

	# Generate config file using debconf
	# and copy to /etc/openstack-dashboard/local_settings.py
	generate_config_file

	# Handle symlinks
	#
	# Symlinks has to be handled here, don't move it to .links file
	# as it can create issues during the update !!!
	# More info as example -> https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=921661
	if [ -L /usr/share/openstack-dashboard/static ]; then
		if ! [ $(readlink -s /usr/share/openstack-dashboard/static) = /var/lib/openstack-dashboard/static ]; then
			ln -sf /var/lib/openstack-dashboard/static /usr/share/openstack-dashboard/static
		fi
	else
		ln -sf /var/lib/openstack-dashboard/static /usr/share/openstack-dashboard/static
	fi

	if [ ! -L /usr/lib/python3/dist-packages/openstack_dashboard/themes ]; then
		ln -sf /usr/share/openstack-dashboard/themes /usr/lib/python3/dist-packages/openstack_dashboard/themes
	fi

	if [ ! -L /usr/lib/python3/dist-packages/openstack_dashboard/local/local_settings.d ]; then
		ln -sf /etc/openstack-dashboard/local_settings.d /usr/lib/python3/dist-packages/openstack_dashboard/local/local_settings.d
	fi

	if ! [ -L /usr/lib/python3/dist-packages/openstack_dashboard/local/local_settings.py ] ; then
		ln -sf /etc/openstack-dashboard/local_settings.py /usr/lib/python3/dist-packages/openstack_dashboard/local/local_settings.py
	fi

	if [ ! -L /usr/lib/python3/dist-packages/openstack_dashboard/conf ]; then
		ln -sf /etc/openstack-dashboard/policy /usr/lib/python3/dist-packages/openstack_dashboard/conf
	fi

	# Some dashboard plugins are not deleting their files under
	# /usr/share/openstack-dashboard/openstack_dashboard/{local,enabled}
	#
	# Anyway, this path is not using anymore because horizon paths was reworked
	# So, let's remove this absolete path
	#
	# This can be removed in future versions.
	if [ -d /usr/share/openstack-dashboard/openstack_dashboard ]; then
		rm -rf /usr/share/openstack-dashboard/openstack_dashboard
	fi

	rebuild_static

	db_stop
fi

if [ "$1" = "triggered" ] ; then
	rebuild_static
fi

#DEBHELPER#

exit 0
