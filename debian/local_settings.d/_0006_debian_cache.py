# Local memory storage is the quickest and easiest session
# back end to set up, as it has no external dependencies whatsoever.
# It has the following significant drawbacks:
#  - No shared storage across processes or workers.
#  - No persistence after a process terminates.
#
# The local memory back end is enabled as the default for Horizon
# solely because it has no dependencies. It is not recommended for
# production use, or even for serious development work.
#
# You can use applications such as Memcached or Redis for external
# caching. These applications offer persistence and shared storage
# and are useful for small-scale deployments and development.
#
# If you comment config below, memcached will be used from
# local_settings.py

CACHES = {
  'default' : {
    'BACKEND': 'django.core.cache.backends.locmem.LocMemCache'
  }
}
